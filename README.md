# Wiki Lemmy France

Cette page liste des ressources pour les utilisateurs de Lemmy, un agrégateur de liens libre. 

## Pour les nouveaux arrivants
- Un lien vers la [FAQ](FAQ.md)

## Les applications mobiles
- La liste complète: https://lemmyapps.netlify.app/

Utilisable peu importe la plateforme: Voyager
- https://vger.app/
- https://m.lemmy.world/ pour lemmy.world

Les plus connues pour Android
- Liftoff
- Jerboa
- Thunder

Les plus connues pour iOS
- Memmy
- Mlem

## Les interfaces alternatives pour ordinateur de bureau
- https://old.lemmy.world/, qui reprend l'interface de old.reddit
- https://photon.xylight.dev/, une interface moderne
- https://alexandrite.app/, interface moderne